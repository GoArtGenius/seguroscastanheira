<?php
/*
 * Home Page
 *
 * Description: Template for displaying the Home Page page (no sidebar, footer or any sections)
 *
 * 
 * @based on:	http://codex.wordpress.org/Page_Templates
 * @depends on: Meta Box plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>

<?php get_header(); ?>



<main>

<?php
	$layout = $ash_options['seguros-homepage-blocks']['enabled'];
	if ($layout): foreach ($layout as $key=>$value) {

		switch($key) {

	        case 'simulacoes': get_template_part( '_templates/_parts/home-blocks/simulacao', 'home' );
	        break;
	        
	        case 'slider': get_template_part( '_templates/_parts/home-blocks/slides', 'home' );
	        break;
	 
	        case 'about': get_template_part( '_templates/_parts/home-blocks/about', 'home' );
	        break;
	 
	        case 'contact': get_template_part( '_templates/_parts/home-blocks/contact', 'home' );  
	        break; 
	        
	        case 'map': get_template_part( '_templates/_parts/home-blocks/map', 'home' );
	        break;

	    }

	}
	endif;
?>	

</main>



<?php get_footer();?>
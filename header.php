<!DOCTYPE html>
<?php
/*
 * The template for displaying the Header
 *
 * 
 * @based on:	http://codex.wordpress.org/Theme_Development
 * @based on:	http://codex.wordpress.org/Function_Reference/get_header
 * @depends on: ---
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>

<html <?php language_attributes(); ?>>

<head>

	<?php ash_head(); ?>

	<?php wp_head(); ?>

</head>


<body <?php body_class(); ?>>











<nav class="navbar navbar-default navbar-fixed-top main-color-bg" role="navigation">



	<!-- Brand and toggle get grouped for better mobile display -->	
	<div class="container">
		
		<div class="navbar-header">

			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class="navbar-brand" href="<?php echo home_url(); ?>">
			
				<a href="<?php echo home_url(); ?>">

				<?php if ($ash_options['nav_logo_on']) { ?>
				
					<?php
					$img_url = $ash_options['nav_logo']['url'];
					$nav_logo = aq_resize( $img_url,900, 100 );
					?>
					
					<img class="menu-logo" src="<?php echo $nav_logo;?>" alt="<?php bloginfo('name'); ?>">

				<?php }
				
				if ($ash_options['nav_title'] == "1" ) {

					bloginfo('name');?>
					
				<?php } ?>

				</a>
				
				<?php if ($ash_options['nav_title'] == "1" ) {?>
					<p><?php bloginfo('description');?></p>
				<?php } ?>
				
			</div>

		</div> <!-- /.navbar-header -->

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div id="navbar-ex1-collapse" class="collapse navbar-collapse navbar-right">
			
			<div class="nav-contact">
				<i class="icon-call"></i> <a href="tel://<?php echo $ash_options['nav-telefone'];?>"><?php echo $ash_options['nav-telefone'];?></a> - <i class="icon-phone"></i> <a href="tel://<?php echo $ash_options['nav-telemovel'];?>"><?php echo $ash_options['nav-telemovel'];?></a>
			</div>
			
			
			
			<ul class="nav navbar-nav navbar-right">
                    <li class="page-scroll">
                        <a href="#simulacao">Simulação Auto</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">Sobre</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Contacto</a>
                    </li>
                </ul>
			
			
			
			<?php //ash_main_nav(); //display the wp_nav_menu?>

			<!--<ul class="nav navbar-nav-main">

			<ul class="nav navbar-nav navbar-right">

				<li>
					<a class="open-search btn btn-link navbar-btn" data-toggle="modal" data-target="#searchModal">
						<i class="icon-search"></i>
					</a>
				</li>

			</ul>-->

		</div> <!-- /#navbar-ex1-collapse -->


	</div> <!-- /.container -->

</nav>

<?php //get_template_part('_templates/parts/search/search','modal');?>



<div id="wrapper">
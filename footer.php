<?php
/*
 * The template for displaying the footer.
 *
 * Contains all footer content (widgets, copyright, wp_nav_menu, ... ) and the closing of #wrapper, </body> and </html>.
 * Contains tracking and other codes added to the options panel in the admin section.
 *
 *
 * @based on:	http://codex.wordpress.org/Template_Hierarchy
 * @depends on: ---
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options ?>


<?php ash_go_top() ?>

<footer role="contentinfo" class="footer">
			
	<div class="wide gray06">
		<div class="container">
			
			<div class="row">
				<div class="col-md-12">
					<?php if( function_exists('ash_social_icons') ) {
						ash_social_icons();
					}?>
				</div>
			</div>
			
			<div id="sidebar-footer1" class="sidebar row clearfix" role="complementary">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-sidebar') ) : endif; ?>
			</div>
			
			
		</div> <!-- /.container -->
	</div> <!-- /.wide -->	
	
	<div class="wide gray07">
		<div class="container">	
			
			<div class="row">
				<div class="col-md-12">
					<?php //ash_footer_nav(); // Adjust using Menus in WordPress Admin ?>
				</div>
		
				<div class="col-md-12 footer-credits">
					<p class="pull-right"><a href="http://enginne.com" id="Enginne" title="By the dudes of Enginne"><small>by Enginne</small></a></p>
					<p class="attribution"><small>&copy; <?php echo date('Y');?> <?php bloginfo('name'); ?> | <em><?php bloginfo('description'); ?></em></small></p>
				</div>
			
			</div>
		
		</div> <!-- /.container -->
	</div> <!-- /.wide -->	

</footer> <!-- /footer -->

</div> <!-- /#wrapper -->

	<?php wp_footer();?>
	 
</body>
</html>
<?php 

// Add LESS to WP
// http://www.noeltock.com/web-design/wordpress/using-less-with-wordpress/
//https://github.com/sanchothefat/wp-less
//include( get_stylesheet_directory(). '/_system/Ash/Core/WP-Less.php' );





if ( ! is_admin() )
{
	add_action( 'wp_enqueue_scripts', 'castanheira_add_scripts', 100 );
}
	
function castanheira_add_scripts()
{

	wp_deregister_script('ash');
	wp_deregister_script('NivoLightbox');
	wp_deregister_style('NivoLightbox');
	wp_deregister_style('NivoLightbox-theme');

	
	//Easing for One Page Scroll
	wp_enqueue_script('easing');


	wp_register_script('castanheira', get_stylesheet_directory_uri() . '/_library/js/castanheira.js', array('jquery'), '1.0', true);
	wp_enqueue_script('castanheira');

	//wp_register_style( 'gvf-b-datepicker', get_stylesheet_directory_uri() . '/_library/less/gravityforms-bs-datepicker.less' );
	//wp_register_style( 'gvf-b', get_stylesheet_directory_uri() . '/_library/less/gravityforms.less' );

	//wp_enqueue_style('gvf-b-datepicker');
	//wp_enqueue_style('gvf-b');

	wp_register_style( 'castanheira', get_stylesheet_directory_uri() . '/style.css', array(), '1.0', 'all' );
	wp_register_style( 'hasteristico', get_stylesheet_directory_uri() . '/_library/css/hasteristico.css', array(), '1.0', 'all' );

	wp_enqueue_style('hasteristico');
	wp_enqueue_style('castanheira');

}








require_once( get_stylesheet_directory().'/_library/modules/FancyBox.php');



// Redux Options

/**
 Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 Simply include this function in the child themes functions.php file.

 NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
 so you must use get_template_directory_uri() if you want to use any of the built in icons

**/

//Remove Sections from parent
add_action( 'after_setup_theme', 'castanheira_undo_ash_hooks' );

function castanheira_undo_ash_hooks()
{
	remove_filter('redux/options/ash_options/sections', 'dynamic_layout_section', null);
	remove_filter('redux/options/ash_options/sections', 'dynamic_style_section', null);
	remove_filter('redux/options/ash_options/sections', 'dynamic_adsense_section', null);
	remove_filter('redux/options/ash_options/sections', 'dynamic_woocommerce_section', null);
}




add_filter('redux/options/ash_options/sections', 'dynamic_castanheira_section', 100);

function dynamic_castanheira_section($sections)
{
	//$sections = array();
	$sections[] = array(
		'title'	=> 'Home Page Seguros',
		'desc'	=> 'Opções da Página Inicial',
		'icon'	=> 'fa fa-umbrella',
		// Leave this as a blank section, no options just some intro text set above.
		'fields' => array(
			array(
			    'id'      => 'seguros-homepage-blocks',
			    'type'    => 'sorter',
			    'title'   => 'Layout',
			    'desc'    => 'Organize how you want the layout to appear on the homepage',
			    'options' => array(
			        'enabled'  => array(
			            'simulacoes'=> 'Simulações',
			            'slider'	=> 'Slider',
			            'about'		=> 'Sobre',
			            'contact'	=> 'Contacto',
			            'map'		=> 'Mapa'
			        ),
			        'disabled' => array(
			        )
			    ),
			),

			

			array(
				'id'		=> 'site-main-colour',
				'type'		=> 'color',
				'mode'		=> 'background-color',
				'output'    => array('.main-color-bg'),
				'title'		=> __('Cor Principal', 'ash'),
				'desc'		=> __('...', 'ash'),
				'transparent'=> false,
				'default' 	=> "#FF8755",
			),
			array(
				'id'		=> 'nav-telefone',
				'type'		=> 'text',
				'title'		=> __('Numero de telefone fixo', 'ash'),
				'desc'		=> __('...', 'ash'),
				'default' 	=> "232 232 232",
			),
			array(
				'id'		=> 'nav-telemovel',
				'type'		=> 'text',
				'title'		=> __('Numero de telemóvel', 'ash'),
				'desc'		=> __('...', 'ash'),
				'default' 	=> "911 999 999",
			),
			
			
			
			array(
			'id'		=> 'slide1-text',
			'type'		=> 'editor',
			'title'		=> __('Texto do primeiro Slide', 'ash'),
			'subtitle'	=> '<h3>Seguro Automovel à sua medida.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at quam velit. In nec sapien elit. Cras varius viverra consectetur. Sed at dolor et orci volutpat pellentesque.</p>',
			'default'	=> '',
			'args'   => array(
				'wpautop'		=> true,
				'teeny'			=> false,
				'textarea_rows'	=> 6
				),
			),
			array(
			'id'		=> 'slide2-text',
			'type'		=> 'editor',
			'title'		=> __('Texto do Segundo Slide', 'ash'),
			'subtitle'	=> '<h3>Seguro Automovel à sua medida.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at quam velit. In nec sapien elit. Cras varius viverra consectetur. Sed at dolor et orci volutpat pellentesque.</p>',
			'default'	=> '',
			'args'   => array(
				'wpautop'		=> true,
				'teeny'			=> false,
				'textarea_rows'	=> 6
				),
			),
		)
	);


	$sections[] = array(
		'icon'		=> 'fa fa-rotate-right',
		'title'		=> __('Secção Simulação', 'ash'),
		'subsection'=> true,
		'fields'	=> array(

			array(
				'id'		=> 'texto-butao-simulacao',
				'type'		=> 'textarea',
				'title'		=> __('Texto ao lado do botão de Simulação', 'ash'),
				'desc'		=> __('...', 'ash'),
				'default' 	=> "Faça aqui e peça a sua simulação",
			),
		),
	);


	$sections[] = array(
		'icon'		=> 'fa fa-copy',
		'title'		=> __('Secção Slides', 'ash'),
		'subsection'=> true,
		'fields'	=> array(

			array(
				'id'          => 'seguros-home-slides',
				'type'        => 'slides',
				'title'       => __('Slides', 'redux-framework-demo'),
				'subtitle'    => __('Unlimited slides with drag and drop sortings.', 'redux-framework-demo'),
				'desc'        => __('This field will store all slides values into a multidimensional array to use into a foreach loop.', 'redux-framework-demo'),
				'placeholder' => array(
					'title'           => __('Título', 'ash'),
					'description'     => __('Texto', 'ash'),
					'url'             => __('Link!', 'ash'),
				),
			),
		),
	);


	$sections[] = array(
		'icon'		=> 'fa fa-info-circle',
		'title'		=> __('Secção Sobre', 'ash'),
		'subsection'=> true,
		'fields'	=> array(

			array(
				'id'		=> 'sobre-text',
				'type'		=> 'editor',
				'title'		=> __('Texto sobre a Empresa', 'ash'),
				'subtitle'	=> __('...', 'ash'),
				'default'	=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at quam velit. In nec sapien elit. Cras varius viverra consectetur. Sed at dolor et orci volutpat pellentesque. Suspendisse volutpat, nibh vitae tempor facilisis, orci nisl faucibus tellus, ut adipiscing purus magna vel orci. Donec ac sodales felis, ac convallis nulla. Sed feugiat ligula id urna accumsan feugiat. Integer sed placerat nisl, elementum lacinia erat. Ut fermentum nulla et risus hendrerit vulputate. In accumsan metus ligula, eu elementum nunc vestibulum sed. Etiam a tortor porttitor elit commodo sollicitudin.',
				'args'   => array(
					'wpautop'		=> true,
					'teeny'			=> false,
					'textarea_rows'	=> 15
				),
			),
		),
	);


	$sections[] = array(
		'icon'		=> 'fa fa-envelope-o',
		'title'		=> __('Secção Contacto', 'ash'),
		'subsection'=> true,
		'fields'	=> array(

			array(
				'id'		=> 'left-contact-text',
				'type'		=> 'editor',
				'title'		=> __('Texto ao lado do Contacto', 'ash'),
				'subtitle'	=> __('...', 'ash'),
				'default'	=> '',
				'args'   => array(
					'wpautop'		=> true,
					'teeny'			=> false,
					'textarea_rows'	=> 10
				),
			),
			array(
				'id'		=> 'seguros-homepage-contact-email',
				'type'		=> 'text',
				'validate'	=> 'email',
				'title'		=> __('Email', 'ash'),
				'subtitle'	=> __('...', 'ash'),
				'default'	=> 'alx.ferreira@gmail.com',
			),
		),
	);
	
	
	$sections[] = array(
		'icon'		=> 'fa fa-map-marker',
		'title'		=> __('Secção Mapa', 'ash'),
		'subsection'=> true,
		'fields'	=> array(

			array(
				'id'		=> 'seguros-homepage-mapa',
				'type'		=> 'text',
				'title'		=> __('Google Map Link', 'ash'),
				'subtitle'	=> __('...', 'ash'),
				'default'	=> 'Santa Comba Dão, Portugal',
			),
		),
	);

	return $sections;

}



/*
 * Required Plugins
 */
 
 add_action( 'tgmpa_register', 'castanheira_register_required_plugins' );

function castanheira_register_required_plugins()
{
	$plugins = array(
		
		//	Forced
		array(
			'name'					=> 'Updater',
			'slug'					=> 'github-updater',
			'source'				=> get_stylesheet_directory() . '/_library/plugins/github-updater.zip',
			'required'				=> true,
			'force_activation'		=> true,
			'force_deactivation'	=> false,
		),
	);

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'			=> 'ash',						// Text domain - likely want to be the same as your theme.
		'default_path'		=> '',							// Default absolute path to pre-packaged plugins
		'parent_menu_slug'	=> 'themes.php',				// Default parent menu slug
		'parent_url_slug'	=> 'themes.php',				// Default parent URL slug
		'menu'				=> 'install-required-plugins',	// Menu slug
		'has_notices'		=> true,						// Show admin notices or not
		'is_automatic'		=> false,						// Automatically activate plugins after installation or not
		'message'			=> 'Some Plugins need activation',							// Message to output right before the plugins table
		'strings'			=> array(
			'page_title'								=> __( 'Install Required Plugins', 'ash' ),
			'menu_title'								=> __( 'Install Plugins', 'ash' ),
			'installing'								=> __( 'Installing Plugin: %s', 'ash' ), // %1$s = plugin name
			'oops'										=> __( 'Something went wrong with the plugin API.', 'ash' ),
			'notice_can_install_required'				=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_install'						=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
			'notice_can_activate_required'				=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_activate'					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
			'notice_ask_to_update'						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_update'						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
			'install_link' 								=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
			'activate_link' 							=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
			'return'									=> __( 'Return to Required Plugins Installer', 'ash' ),
			'plugin_activated'							=> __( 'Plugin activated successfully.', 'ash' ),
			'complete' 									=> __( 'All plugins installed and activated successfully. %s', 'ash' ), // %1$s = dashboard link
			'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa( $plugins, $config );
}	
?>
jQuery(window).load(function() {

	// Remove AutoFocus on Home
	jQuery('input').blur();

});


jQuery(document).ready(function($) {

	// Bootstrap Tip Bottom 
	$('.tip').tooltip({
		placement: "bottom"
	});

	// Search Modal
	$('#searchModal').modal('hide');

	// FancyBox
	$("a#inline-form").fancybox();
	
	
	
	// One Page Scroll
	$('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

	////////////////////////////////////////////////////////////////////////////////////////////
	//GO TO TOP BUTTON
	var $scrolltotop = $("#scroll-top");
		$scrolltotop.css('display', 'none');

	$(function ()
	{
		$(window).scroll(function ()
		{
			if ($(this).scrollTop() > 100)
			{
				$scrolltotop.slideDown('fast');
			} else
			{
				$scrolltotop.slideUp('fast');
			}
		});

		$scrolltotop.click(function ()
		{
			$('body,html').animate(
			{
				scrollTop: 0
			}, 'fast');
			return false;
		});
	});

});



jQuery(document).scroll(function() {    

	var scroll = jQuery(this).scrollTop();
	
	if (scroll >= 50) {
	
		jQuery(".navbar-fixed-top").addClass("navbar-shadow");
	
	} else if (scroll <= 50) {
		
		jQuery(".navbar-fixed-top").removeClass("navbar-shadow");
	
	}

});
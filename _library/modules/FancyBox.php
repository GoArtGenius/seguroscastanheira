<?php
/*
 * Functions for the jQuery Script FancyBox
 *
 *
 * @based on:	http://fancyapps.com/fancybox/
 * @depends on: jQuery FancyBox plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com
 * @link		http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		3.1.5
 * @updated		24.03.2014
 */
global $ash_options;



/*
 * Register and enqueues FancyBox JS & CSS
 *
 * @package		WordPress
 * @subpackage	Ash
 * @since		3.1.5
 * @updated		31.03.2014
 */
add_action( 'wp_enqueue_scripts', 'ash_add_fancybox' );

function ash_add_fancybox()
{
	global $ash_options;

	// FancyBox JS Pack
	wp_register_script('fancybox', get_stylesheet_directory_uri() . '/_library/modules/FancyBox/assets/jquery.fancybox.2.1.5.pack.js',array('mousewheel'),'2.1.5');

	// FancyBox JS dev
	wp_register_script('fancybox-dev', get_stylesheet_directory_uri() . '/_library/modules/FancyBox/assets/jquery.fancybox.2.1.5.js',array('mousewheel'),'2.1.5');

	// FancyBox CSS min
	wp_register_style('fancybox', get_stylesheet_directory_uri() . '/_library/modules/FancyBox/assets/jquery.fancybox.min.css',array(),'2.1.5','all');

	// FancyBox CSS dev
	wp_register_style('fancybox-dev', get_stylesheet_directory_uri() . '/_library/modules/FancyBox/assets/jquery.fancybox.dev.css',array(),'2.1.5','all');


	if ($ash_options['enable_dev_mode'] == '1')
	{
		wp_enqueue_script('fancybox-dev');
		wp_enqueue_style('fancybox-dev');
	} else {
		wp_enqueue_script('fancybox');
		wp_enqueue_style('fancybox');
	}
}


/*
 * Add FancyBox rel "group" to gallery shortcode
 *
 * Based on: http://www.sant-media.co.uk/2010/07/how-to-use-prettyphoto-in-wordpress-galleries/
 *
 * @package		WordPress
 * @subpackage	Ash
 * @since		3.1.5
 * @updated		24.04.2014
 */
add_filter( 'wp_get_attachment_link', 'ash_fancybox_gallery');

function ash_fancybox_gallery ($content)
{
	$content = preg_replace("/<a/","<a rel=\"group\"",$content,1);
	return $content;
}




/*
 * Adds the fancybox jQuery code to the <head>
 *
 * Based on: http://wordpress.transformnews.com/tutorials/how-to-implement-fancybox-in-wordpress-creating-your-own-plugin-84
 *
 * @package		WordPress
 * @subpackage	Ash
 * @since		3.1.5
 * @updated		24.04.2014
 */
 
if ($ash_options['enable_dev_mode'] == '1')
{

	add_action( 'wp_head', 'insert_fancybox_dev' );

	} else {

	add_action( 'wp_head', 'insert_fancybox' );

}


function insert_fancybox()
{
	?>
	<script type="text/javascript">jQuery(document).ready(function($){var select = $('a[href$=".bmp"],a[href$=".gif"],a[href$=".jpg"],a[href$=".jpeg"],a[href$=".png"],a[href$=".BMP"],a[href$=".GIF"],a[href$=".JPG"],a[href$=".JPEG"],a[href$=".PNG"]');select.attr('data-fancybox-group','gallery');select.attr('class','fancybox');select.fancybox();$(".fancybox").fancybox({beforeShow:function(){this.title = $(this.element).find("img").attr("alt");},padding:0,margin:20,wrapCSS:'',openEffect:'elastic',openSpeed:650,arrows:true,closeEffect:'elastic',closeSpeed:650,closeBtn:true,closeClick:false,nextClick:false,prevEffect:'elastic',prevSpeed:650,nextEffect:'elastic',nextSpeed:650,pixelRatio:1,autoSize:true,autoHeight:false,autoWidth:false,autoResize:true,fitToView:true,aspectRatio:false,topRatio:0.5,leftRatio:0.5,scrolling:'auto',mouseWheel:true,autoPlay:false,playSpeed:3000,preload:3,modal:false,loop:true,helpers:{title:{type:'inside',}}});});</script>
	<?php
}

function insert_fancybox_dev()
{?>
	<script type="text/javascript">
	// <![CDATA[
	jQuery(document).ready(function($) {

		//For single images
		var select = $('a[href$=".bmp"],a[href$=".gif"],a[href$=".jpg"],a[href$=".jpeg"],a[href$=".png"],a[href$=".BMP"],a[href$=".GIF"],a[href$=".JPG"],a[href$=".JPEG"],a[href$=".PNG"]'); // select image files
		select.attr('data-fancybox-group', 'gallery'); //data-fancybox-group is used instead of rel tag in html5
		select.attr('class', 'fancybox'); // adds class tag to image
		select.fancybox();

		$(".fancybox").fancybox({
			beforeShow: function () {
				this.title = $(this.element).find("img").attr("alt"); // shows alt tag as image title
			},

			padding:0, //  settings can be removed to use defaults  //
			margin : 20,
			wrapCSS : '',
			openEffect : 'elastic',
			openSpeed : 650,
			arrows : true,
			closeEffect : 'elastic',
			closeSpeed : 650,
			closeBtn : true,
			closeClick : false,
			nextClick : false,
			prevEffect : 'elastic',
			prevSpeed : 650,
			nextEffect : 'elastic',
			nextSpeed : 650,
			pixelRatio: 1, // Set to 2 for retina display support
			autoSize : true,
			autoHeight : false,
			autoWidth : false,
			autoResize : true,
			fitToView : true,
			aspectRatio : false,
			topRatio : 0.5,
			leftRatio : 0.5,
			scrolling : 'auto', // 'auto', 'yes' or 'no'
			mouseWheel : true,
			autoPlay : false,
			playSpeed  : 3000,
			preload : 3,
			modal : false,
			loop : true,
			helpers : {
				title : {
					type : 'inside', // outside
				}
			}
		});

	});
	// ]]>
	</script>
	<?php
}
<?php
/*
 * Template Name: Home Page
 *
 * Description: Template for displaying the Home Page page (no sidebar, footer or any sections)
 *
 * 
 * @based on:	http://codex.wordpress.org/Page_Templates
 * @depends on: Meta Box plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>

<?php get_header(); ?>

<main>

	<div class="container landing-page">
	
		<div id="content" class="clearfix row">
	
			<div id="main" class="col-md-12 clearfix landing" role="main">
	
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

	
					<section class="post_content">
						<?php the_content(); ?>
					</section> <!-- /article section -->
	
	
				</article> <!-- /article -->
	
	
				<?php endwhile; endif; ?>
	
			</div> <!-- /#main -->
	
		</div> <!-- /#content -->
		
		<div class="wide margin60">
			<div class="container">
				<div class="row">
					<div class="well-simulacao">
			
						<div class="col-md-8">
							<h1><?php echo $ash_options['texto-butao-simulacao'];?> <i class="icon-angle-double-right"></i></h1>
						</div>
					
						<div class="col-md-4">
							<a id="inline-form" href="#sim-form" class="btn btn-sunny btn-lg main-color-bg" rel="lightbox" alt="Simulação Auto" title="Simulação Auto">
								<i class="icon-refresh2"></i> Simulação Auto
							</a>
							
							
							<div style="display:none">
								<div id="sim-form">
									<?php echo do_shortcode('[gravityform id="1" name="Simulação Auto" description="false" ajax="true"]');?>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
	</div> <!-- /.container -->
	
	
			
		<div class="wide">
			
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				
				<!-- Indicators -->
				<ol class="carousel-indicators">
			    	<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    	<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				</ol>
			
			  	<!-- Wrapper for slides -->
				<div class="carousel-inner">
					
					<div class="item active" style="background:url('<?php echo get_stylesheet_directory_uri();?>/_library/images/theme/auto-02.jpg') center center; background-size:cover;">
						<div class="carousel-caption">
							<?php echo $ash_options['slide1-text'];?>
						</div>
					</div>
					
					<div class="item" style="background:url('<?php echo get_stylesheet_directory_uri();?>/_library/images/theme/auto-01.jpg') center center; background-size:cover;">
						<div class="carousel-caption">
							<?php echo $ash_options['slide2-text'];?>
						</div>
					</div>
					
			  </div>
			
					<!-- Controls -->
					<a class="left carousel-control" href="#homeCarousel" data-slide="prev">
						<span class="prev-slide">
							<i class="icon-angle-left"></i>
						</span>
					</a>
					<a class="right carousel-control" href="#homeCarousel" data-slide="next">
						<span class="next-slide">
							<i class="icon-angle-right"></i>
						</span>
					</a>

			</div>

		</div>


		
		<div class="wide white" style="padding-top:160px">
			<div class="container">
				<div class="row">


					<div class="wide-title">
						<i class="icon-info"></i> Sobre
					</div>


					<div class="col-md-12 sobre-text">

						<?php echo $ash_options['sobre-text'];?>

					</div>
					
				</div>
			</div>
		</div>		
		
		
		
		<div class="wide white wide-contact">
			<div class="container">
				<div class="row">


					<div class="wide-title">
						<i class="icon-mail"></i> Contacte-nos
					</div>


					<div class="col-sm-6 col-md-6">

						<?php echo $ash_options['left-contact-text'];?>

					</div>

					<div class="col-sm-6 col-md-6">

						<?php // Contact Form Shortcode
							echo do_shortcode('[contact email="alx.ferreira@gmail.com"]');?>

					</div>
				</div>
			</div>
		</div>



		<div class="wide white wide-title">	
			<i class="icon-map-marker"></i> Encontre-nos
		</div>



		<div class="wide">

			<?php // Google Map Shortcode
				echo do_shortcode('[google-map address="Santa Comba Dão, Portugal" width="100%" height="500px"]');?>

		</div>
		
		
		<?php if ($ash_options['enable_share_buttons'] == '1')
{
	ash_share_buttons();
}?>
		
	
	
	
</main>

<?php //if (rwmb_meta('_ash_footer_on') == 0 ) {

	//get_template_part('_templates/parts/footer/minimal','landing-page');

//} else {	

	get_footer();

//} ?>
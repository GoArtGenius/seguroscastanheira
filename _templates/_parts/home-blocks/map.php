<?php
/*
 * Home Block - Contact
 *
 *
 * 
 * @based on:	http://codex.wordpress.org/Page_Templates
 * @depends on: Meta Box plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>


<div class="wide white wide-title" id="map">	
	<i class="icon-map-marker"></i> Encontre-nos
</div>


<div class="wide">

	<?php // Google Map Shortcode
		//echo do_shortcode('[google-map address="Santa Comba Dão, Portugal" width="100%" height="500px"]');
		echo do_shortcode('[google-map address="'. $ash_options['seguros-homepage-mapa'].'" width="100%" height="500px"]');?>

</div>

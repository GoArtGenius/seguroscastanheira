<?php
/*
 * Home Block - Slider
 *
 *
 * 
 * @based on:	http://codex.wordpress.org/Page_Templates
 * @depends on: Meta Box plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>


<div class="wide" id="slides">

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php $i = 0; ?>
			<?php foreach( $ash_options['seguros-home-slides'] as $slide ) : ?>
				<?php if ( $i == 0 ) : ?>
					<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="active"></li>
				<?php else : ?>
					<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>"></li>
				<?php endif; ?>
				<?php $i++; ?>
			<?php endforeach; ?>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<?php $i = 0; ?>
			<?php foreach( $ash_options['seguros-home-slides'] as $slide ) : ?>

				<?php if ( $i == 0 ) : ?>
					<div class="item active" style="background:url('<?php echo $slide['image']; ?>') center center; background-size:cover">
				<?php else : ?>
					<div class="item" style="background:url('<?php echo $slide['image']; ?>') center center; background-size:cover">
				<?php endif; ?>

					<div class="carousel-caption">
						<h3><?php echo $slide['title']; ?></h3>
						<p><?php echo $slide['description']; ?></p>
					</div>

				</div>

				<?php $i++; ?>

			<?php endforeach; ?>

		</div>


		<!-- Controls -->
		<a class="left carousel-control" href="#homeCarousel" data-slide="prev">
			<span class="prev-slide">
				<i class="icon-angle-left"></i>
			</span>
		</a>

		<a class="right carousel-control" href="#homeCarousel" data-slide="next">
			<span class="next-slide">
				<i class="icon-angle-right"></i>
			</span>
		</a>


	</div>

</div>
<?php
/*
 * Home Block - Contact
 *
 *
 * 
 * @based on:	http://codex.wordpress.org/Page_Templates
 * @depends on: Meta Box plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>

<div class="wide white wide-contact" id="contact">

	<div class="container">
		
		<div class="row">

			<div class="wide-title">
				<i class="icon-mail"></i> Contacte-nos
			</div>


			<div class="col-sm-6 col-md-6">
				<?php echo $ash_options['left-contact-text'];?>
			</div>

			<div class="col-sm-6 col-md-6">
				<?php // Contact Form Shortcode
					echo do_shortcode('[contact email="'.$ash_options['seguros-homepage-contact-email'].'"]');?>
			</div>

		</div>

	</div>

</div>
<?php
/*
 * Home Block - About
 *
 *
 * 
 * @based on:	http://codex.wordpress.org/Page_Templates
 * @depends on: Meta Box plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>

<div class="wide white" style="padding-top:160px" id="about">

	<div class="container">

		<div class="row">


			<div class="wide-title">
				<i class="icon-info"></i> Sobre
			</div>


			<div class="col-md-12 sobre-text">

				<?php echo $ash_options['sobre-text'];?>

			</div>
			
		</div>

	</div>

</div>		
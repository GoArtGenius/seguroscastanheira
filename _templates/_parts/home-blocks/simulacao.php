<?php
/*
 * Home Block - Simulação
 *
 *
 * 
 * @based on:	http://codex.wordpress.org/Page_Templates
 * @depends on: Meta Box plugin
 * @credits		---
 * @licence		http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @author		Alex Costa
 * @author-url	http:enginne.com/author/alex-costa/
 * @copyright	Copyright (c) 2014, Enginne.com 
 * @link        http://enginne.com/ash
 * @package 	WordPress
 * @subpackage	Ash
 * @since		1.0
 * @updated		22.03.2014
 */
global $ash_options;?>


<div class="wide margin60" id="simulacao">

	<div class="container">
		
		<div class="row">
			
			<div class="well-simulacao">
			
				<div class="col-md-8">
					<h1><?php echo $ash_options['texto-butao-simulacao'];?> <i class="icon-angle-double-right"></i></h1>
				</div>


				<div class="col-md-4">
					<a id="inline-form" href="#sim-form" class="btn btn-sunny btn-lg main-color-bg" rel="lightbox" alt="Simulação Auto" title="Simulação Auto">
					<i class="icon-refresh2"></i> Simulação Auto <i class="icon-twitter"></i>
				</a>


				<div style="display:none">
					<div id="sim-form">
						<?php echo do_shortcode('[gravityform id="1" name="Simulação Auto" description="false" ajax="true"]');?>
					</div>
				</div>


			</div>

		</div>

	</div>

</div>